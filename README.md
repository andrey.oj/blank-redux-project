## Creates a blank project  

**`.babelrc`**  
**`.eslintignore`**  
**`.gitignore`**  
**`package.json`**  
**`rc.config.json`**  
**`webpack.config.js`**  
**`README.md`**  
**`src`**  
- `App.js`  
- `colorshema.js`   
- `index.js`    
- **`redux`**  
| `configureStore.js`  
| `rootLogic.js`  
| `rootReducer.js`  
- **`pages`**  
| `Routes.js`  
|**`home`**  
/-/**`components`**  
/---| `HomePage.js`  
/---| `...`  
/-/**`redux`**  
/---| `homeTypes.js`  
/---| `homeActions.js`  
/---| `homeReducer.js`  
/---| `homeLogic.js`  
|**`example`**  
/-/**`components`**  
/---| `ExamplePage.js`  
/---| `...`  
/-/**`redux`**  
/---| `exampleTypes.js`  
/---| `...`  

**`public`**  
- `favicon.ico`  
- `manifest.json`   
- `index.html`   

**`test`**
- `App.test.js`



## Installing
Create new project dir  
`mkdir myproject`

Clone the project  
`git clone git@gitlab.com:andrey.oj/blank-redux-project.git myproject`  

Go into that dir 
`cd myproject`

Install dependencies  
`yarn install`  

Start dev server  
`yarn dev`

### Available Scripts
Starts development webpack server  
`yarn dev` 

Builds the project into `dist/`  
`yarn build`  

Creates `{name}Reducer.js`, `{name}Logic.js`, `{name}Actions.js`, `{name}Types.js` in **`pages/{name}`** and  
binds them into `rootReducer.js` and `rootLogic.js`  
`yarn new-flow {name}`  

Create a new action, bind in reducer, start name with package  
`yarn new-action flowname_some_action`

Prettifies your code  
`yarn pretty`

Lints and fixes your code, see config in `.eslinrc.json`  
`yarn lint`  

Runs the tests  
`yarn test`

### Thank you for using my template
I hope you like it
  
_Developer_:**`Andrey Paramonov`**