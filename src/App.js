import '@babel/polyfill';
import 'antd/dist/antd.less';
import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Routes from './pages/Routes';

import { configureStore } from './redux/configureStore';

const App = () => (
    <Provider store={configureStore({})}>
        <BrowserRouter>
            <Routes />
        </BrowserRouter>
    </Provider>
);

export default App;
