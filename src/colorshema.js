module.exports = {
    Theme: '#3461aC',
    Link: '#24519C',

    Positive: '#63AE01',
    Negative: '#E95334',
    Neutral: '#FFA62C',

    Background: '#fafafa'
};
