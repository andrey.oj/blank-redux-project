import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './home/components/Home';
import PageNotFound from './home/components/PageNotFound';

const Routes = () => (
    <Switch>
        <Route exact path="/" component={Home} />
        <Route path="*" component={PageNotFound} />
    </Switch>
);

export default Routes;
