import React from 'react';
import { Card, Row, Col, Icon, Select, Button, Progress, Tag } from 'antd';
import { connect } from 'react-redux';
import { exampleDoData } from '../redux/homeActions';

const ExampleComponent = () => (
    <Card title="Home">
        <Row span={16}>
            <Col span={24}>
                <h1>I like and design so here are some easy examples</h1>
                <h2>
                    Visit{' '}
                    <a href="https://ant.design/components/">
                        <Icon type="ant-design" /> Ant design
                    </a>{' '}
                    page
                </h2>
            </Col>

            <Col span={6}>
                <h2>Buttons</h2>
                <Button.Group>
                    <Button type="primary">Primary</Button>
                    <Button>Default</Button>
                    <Button type="dashed">Dashed</Button>
                    <Button type="danger">Danger</Button>
                </Button.Group>
            </Col>
            <Col span={6}>
                <h2>Select</h2>
                <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Select a person"
                    optionFilterProp="children"
                    onChange={p => alert(`Selected ${p}`)}
                    filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                >
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                </Select>
            </Col>
            <Col span={6}>
                <h2>Progress bars...</h2>
                <Progress type="circle" percent={75} />
            </Col>
            <Col span={6}>
                <h2>Tags...</h2>
                <Tag color="magenta">Is</Tag>
                <Tag color="red">this</Tag>
                <Tag color="volcano">not</Tag>
                <Tag color="orange">beautiful</Tag>
                <Tag color="gold">?</Tag>
            </Col>
        </Row>
    </Card>
);

const mapStateToProps = state => ({
    test: state.home.test
});

const mapDispatchToProps = dispatch => ({
    exampleDoData: () => dispatch(exampleDoData())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExampleComponent);
