import React from 'react';
import { Card } from 'antd';
import Home from '../components/Home';

const styleDiv = {
    textAlign: 'center'
};

const PageNotFound = () => (
    <Card>
        <div style={styleDiv}>
            <h1>404 - Page does not exist</h1>
        </div>
        <Home />
    </Card>
);

export default PageNotFound;
