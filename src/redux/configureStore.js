import { applyMiddleware, createStore } from 'redux';
import { createLogicMiddleware } from 'redux-logic';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootLogic from './rootLogic';
import rootReducer from './rootReducer';

export const configureStore = (preloadedState = {}) => {
    const deps = {
        /*
         * TODO add your dependencies here
         */
    };

    const logicMiddleware = createLogicMiddleware(rootLogic, deps);
    return createStore(
        rootReducer,
        preloadedState,
        composeWithDevTools({})(applyMiddleware(logicMiddleware))
    );
};

export default { configureStore };
