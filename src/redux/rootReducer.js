import { combineReducers } from 'redux';
import home from '../pages/home/redux/homeReducer';

const rootReducer = combineReducers({
    home
});

export default rootReducer;
