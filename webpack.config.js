const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { DefinePlugin } = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Colors = require('./src/colorshema');

const htmlPlugin = new HtmlWebPackPlugin({
  template: './public/index.html',
  filename: './index.html',
});
const cssPlugin = new MiniCssExtractPlugin({
  filename: '[name].css',
  chunkFilename: '[id].css',
});

const copyPlugin = new CopyWebpackPlugin([
  { from: 'public', to: 'public' },
]);

const envPlugin = new DefinePlugin({
  'process.env': {
    ENV: JSON.stringify('DEV'),

  },
});


module.exports = () => ({
  entry: './src/index.js',
  mode: 'production',
  output: {
    path: `${__dirname}/dist`,
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/, /(dist)/],
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.less$/,
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader', // translates CSS into CommonJS
        }, {
          loader: 'less-loader', // compiles Less to CSS
          options: {
            modifyVars: {
              'primary-color': Colors.Theme,
              'link-color': Colors.Link,
              'success-color': Colors.Positive,
              'error-color': Colors.Negative,
              'warning-color': Colors.Neutral,
              'border-radius-base': '2px',
              'text-color': 'rgba(0, 0, 0, .65)',
              'text-color-secondary': 'rgba(0, 0, 0, .45)',
            },
            javascriptEnabled: true,
          },
        }],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.css'],
  },
  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
  },
  plugins: [
    htmlPlugin,
    cssPlugin,
    copyPlugin,
    envPlugin,
  ],


});
